import { Component } from "react";
import styled from "styled-components";
import PropTypes from 'prop-types';

const CountFav = styled.div`
    position: absolute;
    bottom: -8px;
    left: 55px
`;
class CounterFavorite extends Component {

    actualCount = () => {
        const { countFavorite } = this.props;
        if (localStorage.getItem('arrayFavorite') === null) {
            return countFavorite;
        }
        return JSON.parse(localStorage.getItem('arrayFavorite')).length;
    }

    render() {
        const { countFavorite } = this.props
        return (
            <CountFav>
                <p>{this.actualCount()}</p>
            </CountFav>
        )
    }
}

CounterFavorite.propTypes = {
    countFavorite: PropTypes.number,
}
export default CounterFavorite;