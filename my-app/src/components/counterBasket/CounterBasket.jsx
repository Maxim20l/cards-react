import { Component } from "react";
import styled from "styled-components";
import PropTypes from 'prop-types';


const CountBasket = styled.div`
    position: absolute;
    bottom: -8px;
    left: 16px
`;

class CounterBasket extends Component {
    actualCount = () => {
        const { countBasket } = this.props;
        if (localStorage.getItem('arrayBasket') === null) {
            return countBasket;
        }
        return JSON.parse(localStorage.getItem('arrayBasket')).length;
    }

    render() {
        const { countBasket } = this.props
        return (
            <CountBasket>
                <p>{this.actualCount()}</p>
            </CountBasket>
        )
    }
}

CounterBasket.propTypes = {
    countBasket: PropTypes.number,
}

export default CounterBasket;