import styled from "styled-components";
import { IoIosBasket, IoIosIceCream } from "react-icons/io"
import { Container } from "../Container";
import { Component } from "react";
import { CounterFavorite } from "../counterFavorite";
import { CounterBasket } from "../counterBasket";
import PropTypes from 'prop-types';

const HeaderEl = styled.header`
    position: fixed;
    width: 100%;
    background-color: #FEC77E;
`;
const Wrapper = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    gap: 6px;
`;
const LogoName = styled.a.attrs({
    href: '#',
})`
    font-family: 'Roboto';
    font-style: normal;
    font-weight: 700;
    font-size: 1.8rem;
    text-decoration:none;
    color: #CC2A41;
    text-align: center;
    padding: 10px;
`;
const ContactUsBtn = styled.a.attrs({
    href: '#',
})`
    font-family: 'Roboto';
    font-style: normal;
    font-weight: 700;
    font-size: 19px;
    text-decoration: none;
    text-align: center;
    border: 1px solid black;
    border-radius: 8px;
    padding: 6px;
    color: white;
    background-color: #CC2A41;
    border-width: 0px;
    transition: 0.9s;

    &:hover {
        color: #CC2A41;
        background: white;
    }
`;
const Basket = styled.a.attrs({
    href: '#'
})``;
const Favorite = styled.a.attrs({
    href: '#'
})``;

class Header extends Component {
    render() {
        const { countFavorite, countBasket } = this.props;
        return (
            <>
                <div className="bgImg">
                    <HeaderEl>
                        <Container>
                            <Wrapper>
                                <ContactUsBtn>Contact Us</ContactUsBtn>
                                <LogoName>The Magic Slab</LogoName>
                                <div className="iconsContainer">
                                    <Basket>
                                        <IoIosBasket />
                                    </Basket>
                                    <CounterBasket countBasket={countBasket}/>
                                    <CounterFavorite countFavorite={countFavorite}/>
                                    <Favorite>
                                        <IoIosIceCream />
                                    </Favorite>
                                </div>
                            </Wrapper>
                        </Container>
                    </HeaderEl>
                </div>
            </>
        )
    }

}

Header.propTypes = {
    countBasket: PropTypes.number,
    countFavorite: PropTypes.number,
}

export default Header;