import styled from "styled-components";
import { Component } from "react";
import { Card } from "./card/Card";
import { Container } from "../Container";
import PropTypes from 'prop-types';


const ContainerCards = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    gap: 8px
`

class Cards extends Component {

    render() {
        const { cardsArr, isFavorite, isUnFavorite, changeStarFavorite, arrProductsFavorite, openModalToBuy } = this.props;
        const card = cardsArr.map(elem => {
            return <Card key={elem.articul} data={elem} fullStar = {isFavorite} arrProductsFavorite = { arrProductsFavorite } emptyStar = {isUnFavorite} changeStarFavorite = {changeStarFavorite} openModalToBuy={openModalToBuy}/>;
        })
       
        return (
            <>
                <Container>
                    <ContainerCards>
                        {card}
                    </ContainerCards>
                </Container>
            </>
        )
    }
}



Cards.propTypes = {
    cardsArr: PropTypes.array,
    isFavorite: PropTypes.bool,
    isUnFavorite: PropTypes.bool,
    changeStarFavorite: PropTypes.func,
    arrProductsFavorite: PropTypes.string,
    openModalToBuy: PropTypes.func
}
export default Cards;