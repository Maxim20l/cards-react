import { Component } from "react";
import styled from "styled-components";
import { IoStar, IoStarOutline} from "react-icons/io5";
import { BtnOpenModal } from '../../buttonOpenModalBuy';
import PropTypes from 'prop-types';


const Container = styled.div`
    background: #e5d1ca;
    width: 250px;
    display: flex;
    flex-direction: column;
    align-items: center;
    border: 1px solid black;
    border-width: 0px;
    box-shadow: 7px 6px 4px grey;
    border-radius: 10px;
    margin-top: 10px;
`

const Image = styled.img`
    width: 200px;
    height: 200px;
    border: 1px solid black;
    border-width: 0px;
    border-radius: 80px;
`
const WrapperImg = styled.div`
    padding: 10px;
`
const Star = styled.div`
    width: 25px
` 

class Card extends Component {

    
    render() {
        const {
            name,
            price,
            img,
            articul,
            color
        } = this.props.data
        const {changeStarFavorite, arrProductsFavorite, openModalToBuy} = this.props;
        return (
            <>
                <Container id={articul}>
                    <WrapperImg>
                        <Star onClick={changeStarFavorite}>
                            {!arrProductsFavorite.includes(articul) && <IoStarOutline />}
                            {arrProductsFavorite.includes(articul) && <IoStar />}
                        </Star>
                        <Image src={img} alt={color}></Image>
                    </WrapperImg>
                    <div className="inform">
                        <p>{name}</p>
                        <p>{price}</p>
                        <p>{color}</p>
                    </div>
                    <BtnOpenModal openModalToBuy={openModalToBuy}/>
                </Container>
            </>
        )
    }
}

Card.propTypes = {
    arrProductsFavorite: PropTypes.string,
    changeStarFavorite: PropTypes.func,
    openModalToBuy: PropTypes.func
}

export { Card }