
import './App.css';
import { Header } from './components/header';
import { Cards } from './components/cards';
import { Component } from 'react';
import { ModalConfirmToBuy } from './components/modalConfirmToBuy';

let arrFavorite = () => {
  if (localStorage.getItem('arrayFavorite') === null) {
    return '[]';
  }
  return localStorage.getItem('arrayFavorite');
}




class App extends Component {
  state = {
    arrCards: [],
    isFavorite: false,
    isUnFavorite: true,
    arrProductsFavorite: arrFavorite(),
    countFavorite: 0,
    countBasket: 0,
    productToBuy: 0,
    isModalToBuy: false,
  }



  componentDidMount() {
    const sendRequest = async (url) => {
      const response = await fetch(url);
      const result = await response.json();
      this.setState((prevState) => {
        return {
          ...prevState,
          arrCards: result.product
        }
      })
      return result.product;
    }
    sendRequest('./iceCreamElement.json');
  }


  changeStarFavorite = (event) => {

    if (!localStorage.getItem('arrayFavorite')) {
      localStorage.setItem('arrayFavorite', JSON.stringify([]));
    }

    if (event.target.parentElement.tagName == 'svg') {
      const parentImg = event.target.parentElement;
      const parentParentImg = parentImg.parentElement;
      const wrapperParentimg = parentParentImg.parentElement;
      const product = wrapperParentimg.parentElement.id;
      const newArr = JSON.parse(this.state.arrProductsFavorite);
      if (!JSON.parse(this.state.arrProductsFavorite).includes(product)) {
        newArr.push(product);
      }

      localStorage.setItem('arrayFavorite', JSON.stringify(newArr))
      this.setState((prevState) => {
        return {
          ...prevState,
          arrProductsFavorite: localStorage.getItem('arrayFavorite'),
        }
      })
    } else {
      const parentImg = event.target.parentElement;
      const parentParentImg = parentImg.parentElement;
      const product = parentParentImg.parentElement.id;
      const newArr = JSON.parse(this.state.arrProductsFavorite);
      if (!JSON.parse(this.state.arrProductsFavorite).includes(product)) {
        newArr.push(product);
      }
      localStorage.setItem('arrayFavorite', JSON.stringify(newArr))
      this.setState((prevState) => {
        return {
          ...prevState,
          arrProductsFavorite: localStorage.getItem('arrayFavorite'),
        }
      })
    }

  }

  openModalToBuy = (event) => {

    this.setState((prevState) => {
      return {
        ...prevState,
        productToBuy: event.target.parentElement.id,
        isModalToBuy: !prevState.isModalToBuy
      }
    })
    console.log(event.target.parentElement.id)
  }

  closeModalToBuy = (event) => {
    event.stopPropagation();
    this.setState((prevState) => {
      return {
        ...prevState,
        isModalToBuy: !prevState.isModalToBuy,
      }

    })
  }

  closeModals = (event) => {

    if (this.state.isModalToBuy === true) {
      event.stopPropagation();
      if (event.target.id === 'modal-wrapper') {
        this.setState((prevState) => {
          return {
            ...prevState,
            isModalToBuy: !prevState.isModalToBuy
          }
        })
      }
    }
  }

  putElementIntoBasket = () => {
    if (localStorage.getItem('arrayBasket') === null) {
      localStorage.setItem('arrayBasket', JSON.stringify([]));
    }
    const arrProductsToBuy = JSON.parse(localStorage.getItem('arrayBasket'));
    arrProductsToBuy.push(this.state.productToBuy);
    localStorage.setItem('arrayBasket', JSON.stringify(arrProductsToBuy));
    this.setState((prevState) => {
      return {
        ...prevState,
        isModalToBuy: !prevState.isModalToBuy
      }
    })
  }
  render() {
    const { isFavorite, isUnFavorite, arrCards, arrProductsFavorite, countFavorite, isModalToBuy, countBasket } = this.state;
    return (
      <>
        <Header countFavorite={countFavorite} countBasket = {countBasket}/>
        <Cards cardsArr={arrCards} openModalToBuy={this.openModalToBuy} isFavorite={isFavorite} isUnFavorite={isUnFavorite} arrProductsFavorite={arrProductsFavorite} changeStarFavorite={this.changeStarFavorite}/>
        {isModalToBuy && <ModalConfirmToBuy putElementIntoBasket={this.putElementIntoBasket} closeModalToBuy={this.closeModalToBuy} closeModals={this.closeModals} />}
      </>
    )
  }


}

export default App;
